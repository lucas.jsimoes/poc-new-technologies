package main

import (
	"log"
	"os"
)

func main() {
	log.Println("Starting Server...")
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
