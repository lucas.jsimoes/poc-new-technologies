package main

import (
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "bank",
	Short: "Bank POC",
	Run: func(c *cobra.Command, args []string) {
		server, err := NewServer()
		if err != nil {
			os.Exit(1)
		}
		defer Shutdown(server)
	},
}
