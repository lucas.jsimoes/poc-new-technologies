package main

import (
	"bank/internal/libs/middleware"
	transactiongrpcclient "bank/internal/transaction-system/data-provider/client/grpc"
	transactionrepository "bank/internal/transaction-system/data-provider/repository"
	transactionrest "bank/internal/transaction-system/entrypoint/rest"
	transactionservice "bank/internal/transaction-system/service"
	userrepository "bank/internal/user-system/data-provider/repository"
	usergrpc "bank/internal/user-system/entrypoint/grpc"
	userproto "bank/internal/user-system/entrypoint/grpc/proto"
	userrest "bank/internal/user-system/entrypoint/rest"
	userservice "bank/internal/user-system/service"
	walletgrpc "bank/internal/wallet-system/entrypoint/grpc"
	"log"
	"net"

	walletrepository "bank/internal/wallet-system/data-provider/repository"
	walletproto "bank/internal/wallet-system/entrypoint/grpc/proto"
	walletrest "bank/internal/wallet-system/entrypoint/rest"
	walletservice "bank/internal/wallet-system/service"

	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
	"google.golang.org/grpc"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	_ "github.com/lib/pq"
)

// Server ...
type Server struct {
	router     *mux.Router
	negroni    *negroni.Negroni
	gormDB     *gorm.DB
	grpcServer *grpc.Server
	grpcClient *grpc.ClientConn
	closes     []func() error
}

// NewServer ...
func NewServer() (*Server, error) {
	server := new(Server)

	server.grpcClient = server.runGrpcClient()

	server.router = mux.NewRouter()
	server.grpcServer = grpc.NewServer()

	db, err := newPostgresConnectionGorm()
	if err != nil {
		log.Printf("\nCannot connection with postgres: %+v", err)

		return nil, err
	}
	server.gormDB = db

	server.loadDependencies()

	server.negroni = negroni.New(middleware.EventTrackingRest())
	server.negroni.UseHandler(server.router)

	go server.runGrpcServer()
	server.runHttpServer()

	return server, nil
}

// runHttpServer ...
func (s *Server) runHttpServer() {
	if err := http.ListenAndServe(":8080", s.negroni); err != nil {
		log.Fatalf("Cannot run the server http: %v", err)
	}
}

// runGrpcServer ...
func (s *Server) runGrpcServer() {
	lis, err := net.Listen("tcp", ":8200")
	if err != nil {
		log.Printf("GRPC failed to listen: %v", err)
	}

	if err := s.grpcServer.Serve(lis); err != nil {
		log.Fatalf("Failed to server grpc: %v", err)
	}
}

// runGrpcClient ...
func (s *Server) runGrpcClient() *grpc.ClientConn {
	conn, err := grpc.Dial("localhost:8200", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Failed to client grpc: %v", err)
	}

	return conn
}

// loadDependencies ...
func (s *Server) loadDependencies() {
	s.loadUserDependencies()

	s.loadWalletDependencies()

	s.loadTransactionDependencies()
}

// loadWalletDependencies
func (s *Server) loadWalletDependencies() {
	walletService := walletservice.NewWalletService(
		walletrepository.LoadWalletRepositories(s.gormDB),
		func() time.Time {
			return time.Now()
		},
	)

	walletRest := walletrest.NewWalletRest(
		walletService,
		s.router,
	)
	walletRest.RegisterRoutes()

	walletGRPCServer := walletgrpc.NewWalletServiceServer(walletService)
	walletproto.RegisterWalletServiceServer(s.grpcServer, walletGRPCServer)
}

// loadUserDependencies
func (s *Server) loadUserDependencies() {
	userService := userservice.NewUserService(
		userrepository.LoadUserRepositories(s.gormDB),
		func() time.Time {
			return time.Now()
		},
	)

	userRest := userrest.NewUserRest(
		userService,
		s.router,
	)
	userRest.RegisterRoutes()

	userGRPCServer := usergrpc.NewUserServiceServer(userService)
	userproto.RegisterUserServiceServer(s.grpcServer, userGRPCServer)
}

// loadTransactionDependencies
func (s *Server) loadTransactionDependencies() {
	transactionUserGRPCClient := transactiongrpcclient.NewTransactionUserGRPCClient(
		userproto.NewUserServiceClient(s.grpcClient),
	)

	transactionWalletGRPCClient := transactiongrpcclient.NewTransactionWalletGRPClient(
		walletproto.NewWalletServiceClient(s.grpcClient),
	)

	transactionService := transactionservice.NewTransactionService(
		transactionrepository.LoadTransactionRepositories(s.gormDB),
		*transactionUserGRPCClient,
		*transactionWalletGRPCClient,
		func() time.Time {
			return time.Now()
		},
	)

	transactionRest := transactionrest.NewTransactionRest(
		transactionService,
		s.router,
	)
	transactionRest.RegisterRoutes()
}

// Shutdown ...
func Shutdown(server *Server) {
	for _, close := range server.closes {
		if err := close(); err != nil {
			log.Println("Cannot close function")
		}
	}
}

// newPostgresConnectionGorm ...
func newPostgresConnectionGorm() (*gorm.DB, error) {
	dbURL := "postgres://admin:123@localhost:5432/bank_db"

	db, err := gorm.Open(postgres.Open(dbURL), &gorm.Config{})

	if err != nil {
		log.Printf("\nCannot connect with postgres gorm error: %+v", err)

		return nil, err
	}

	return db, nil
}
