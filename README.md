# Bank proof of concept

- This system was created to apply and test new or current technologies that the
market is using in a scalable way spanning software architecture, system design, 
authentication, external/internal communication protocols, observability,
Application performance management (APM), monitoring, api gateway, infrastructure 
and static code analysis.

## Topics 

- [1 System Architecture](#1-System-Architecture) 
    * [1.1 Bounded Contexts](#11-Bounded-Contexts)
    * [1.2 Clean Architecture](#12-Clean-Architecture)
- [2 What does the Application do?](#2-What-does-the-Application-do)
    * [2.1 Main Flow](#21-Main-Flow)
    * [2.2 Tests](#22-Tests)
        + [2.2.1 Unit Tests](#221-Unit-Tests)
        + [2.2.2 Integration Tests](#222-Integration-Tests)
- [3 Communication Protocols](#1-Communication-Protocols)
    * [3.1 Http Representational State Transfer (REST)](#31-Http-Representational-State-Transfer-(REST))
        + [3.1.1 Gorilla mux](#311-Gorilla-mux)
        + [3.1.2 Negroni](#311-Negroni)
    * [3.2 Http2 Google Remote Procedure Call (GRPC)](#32-Http2-Google-Remote-Procedure-Call-(GRPC))
- [X TODO IMPROVEMENTS](#X-TODO-IMPROVEMENTS)

## 1 System Architecture
- An architectural pattern is a studied, tested, and documented solution to a 
recurring problem. The model helps in making software design decisions, such as 
what its usefulness will be and the functions and relationships of each subsystem.

### 1.1 Bounded Contexts
- Why use the bounded contexts(DDD) pattern? Thinking about a strategy of new companies 
such as startups with limited resources without a completely defined, it would 
be interesting to create a monolithic architecture but with a separation limited
to possible splits in the future in such a way that it is structured by 
application domains so when you have the need for this separation for more 
scalable microservices to be easily and already structured (split and play). 
For this to work in a simple way it was implemented exclusive internal communication
between domains of internal systems using GRPC (Google Remote Procedure Call) 
makes split and play even easier.

### 1.2 Clean Architecture
- Clean Architecture is an architectural pattern proposed by Robert Martin with 
the aim of promoting the implementation of systems that favor code reusability, 
cohesion, technology independence and testability.

![alt text](readme-images/cleanarc.png)

Clean Architecture, applied in a very simple way:

Application Packages     | Clean Architecture
-------------------------| -------------
domain                   | entities
service                  | use cases
entrypoint               | adapters
data-provider            | frameworks and drivers

## 2 What does the Application do?
- The idea was to create a simple application in such a way that it is possible to 
apply all and any technology desired to enrich the experience of working with
the same.

### 2.1 Main Flow

![alt text](readme-images/main-flow.png)

## 2.2 Tests

### 2.2.1 Unit Tests
- To work in a simpler and faster way managing mocks mockery was used which is a
  framework that generates mocks from interfaces facilitating its use in unit tests.
<br>```example: mockery --name Wallet --recursive --case=underscore --output ./internal/wallet-system/mock-interface```
- x% of code coverage in the service layer.   
- x% of code coverage in the entrypoint layer.
- x% of code coverage in the data provider layer.

### 2.2.2 Integration Tests
- Each and every integrated test starts from the repository layer in order to
  ensure that any and all communication with the database is done as expected.
- x% of code coverage in the data provider layer - repository.

## 3 Communication Protocols
- Communication protocols are responsible for ensuring internal communication or 
external of our system in a healthy and safe way, thinking about this project 
the idea is to use some of the more popular ones like REST and GRPC.

### 3.1 Http Representational State Transfer (REST)
- For this project, the use of REST in the entrypoint layer was architected in order to
be a gateway to the system, ensuring stable and secure communication with 
applications, web frontend and others.

### 3.1.1 Gorilla mux
- Gorilla mux was used for route management and http service facilitating and making 
the settings related to REST APIs cleaner.

### 3.1.2 Negroni
- Negroni was used to have the responsibility of making the interception or wrapper 
of any request and apply a RequestID in the context of the request enabling tracking 
of any and all requisitions.

### 3.2 Http2 Google Remote Procedure Call (GRPC)
- For this project, at the internal communication level, thinking about the 
application of the bounded context was used grpc for any and all information 
transfer between internal systems and with that, in addition to making the application
more scalable for a possible split in the future we guarantee faster communication due
to GPRC to use http2 that traffics binary in its requests or streaming.

## X TODO IMPROVEMENTS
- Backlog
    * RequestID
    * ZapLog
    * OpenTelemetry
    * NewRelic
    * Grafana
    * Datadog
    * API Gateway
    * Autentication(Keycloak)
    * SonarQube
    * Kubernetes
    * Service Mesh com Istio
