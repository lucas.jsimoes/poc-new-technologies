package domain

import "time"

// Transaction ...
type Transaction struct {
	ID        string    `json:"id" gorm:"primaryKey"`
	UserID    string    `json:"user_id"`
	Amount    float64   `json:"amount"`
	CreatedAt time.Time `json:"created_at"`
}

func (Transaction) TableName() string {
	return "transaction_system.transaction"
}
