package rest

import (
	"bank/internal/transaction-system/service"

	"github.com/gorilla/mux"
)

// Transaction ...
type Transaction struct {
	service *service.TransactionService
	router  *mux.Router
}

// NewTransactionRest ...
func NewTransactionRest(
	service *service.TransactionService,
	router *mux.Router,
) *Transaction {
	subRouter := router.PathPrefix("/transaction").Subrouter()
	return &Transaction{
		service: service,
		router:  subRouter,
	}
}

// RegisterRoutes ...
func (t Transaction) RegisterRoutes() {
	t.router.HandleFunc(
		"", t.create,
	).Headers(
		"Content-Type", "application/json",
	).Methods("POST")
}
