package rest

import (
	"bank/internal/transaction-system/domain"
	"bank/internal/transaction-system/entrypoint/rest/event"
	"log"

	//wallet "bank/internal/wallet-system/service"
	"encoding/json"
	"net/http"
)

// create ...
func (t *Transaction) create(
	w http.ResponseWriter, r *http.Request,
) {
	var err error
	var payload event.Transaction

	decoder := json.NewDecoder(r.Body)
	if err = decoder.Decode(&payload); err != nil {
		log.Printf("\nfunc:create(Transaction.Rest) invalid request body, error: %+v", err)

		w.WriteHeader(http.StatusBadRequest)
		return
	}

	id, err := t.service.Create(transactionToDomain(payload))
	if err != nil {
		log.Printf("\nfunc:create(Transaction.Rest) called service err: %+v", err)

		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(id)
	return
}

// transactionToDomain ...
func transactionToDomain(payload event.Transaction) domain.Transaction {
	return domain.Transaction{
		UserID: payload.UserID,
		Amount: payload.Amount,
	}
}
