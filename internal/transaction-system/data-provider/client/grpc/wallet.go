package grpc

import (
	walletproto "bank/internal/wallet-system/entrypoint/grpc/proto"
	"context"
	"log"
)

type TransactionWalletGRPC interface {
	Withdrawal(userID string, amount float64) (bool, error)
}

// TransactionWalletGRPClient ...
type TransactionWalletGRPClient struct {
	walletGrpcClient walletproto.WalletServiceClient
}

// NewTransactionWalletGRPClient ...
func NewTransactionWalletGRPClient(
	walletGrpcClient walletproto.WalletServiceClient,
) *TransactionWalletGRPClient {
	return &TransactionWalletGRPClient{
		walletGrpcClient: walletGrpcClient,
	}
}

// Withdrawal ...
func (w *TransactionWalletGRPClient) Withdrawal(
	userID string, amount float32,
) (bool, error) {
	withdrawal, err := w.walletGrpcClient.Withdrawal(
		context.Background(),
		&walletproto.WalletRequest{UserID: userID, Amount: amount},
	)
	if err != nil {
		log.Printf("\nfunc:Withdrawal(Transaction.GRPCClient) error: %+v", err)

		return false, err
	}

	return withdrawal.Withdrawn, err
}
