package grpc

import (
	userproto "bank/internal/user-system/entrypoint/grpc/proto"
	"context"
	"log"
)

type TransactionUserGRPC interface {
	UserExists(userID string) (bool, error)
}

// TransactionUserGRPCClient ...
type TransactionUserGRPCClient struct {
	userGrpcClient userproto.UserServiceClient
}

// NewTransactionUserGRPCClient ...
func NewTransactionUserGRPCClient(
	userGrpcClient userproto.UserServiceClient,
) *TransactionUserGRPCClient {
	return &TransactionUserGRPCClient{
		userGrpcClient: userGrpcClient,
	}
}

// UserExists ...
func (u *TransactionUserGRPCClient) UserExists(userID string) (bool, error) {
	existUser, err := u.userGrpcClient.UserExists(
		context.Background(), &userproto.UserRequest{UserID: userID},
	)
	if err != nil {
		log.Printf("\nfunc:UserExists(Transaction.GRPCClient) error: %+v", err)

		return false, err
	}

	return existUser.Exists, err
}
