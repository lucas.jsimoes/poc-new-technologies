package repository

import (
	"bank/internal/transaction-system/domain"

	"gorm.io/gorm"
)

// Transaction ...
type Transaction interface {
	Create(transaction domain.Transaction) (string, error)
}

// TransactionPostgres ...
type TransactionPostgres struct {
	db *gorm.DB
}

// BuildTransactionPostgres ...
func BuildTransactionPostgres(
	db *gorm.DB,
) TransactionPostgres {
	return TransactionPostgres{db: db}
}

// Create ...
func (u TransactionPostgres) Create(transaction domain.Transaction) (string, error) {
	result := u.db.Create(&transaction)
	if result.Error != nil {
		return "", result.Error
	}

	return transaction.ID, result.Error
}
