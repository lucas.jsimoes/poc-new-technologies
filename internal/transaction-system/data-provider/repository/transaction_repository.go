package repository

import (
	"gorm.io/gorm"
)

// TransactionRepositories ...
type TransactionRepositories struct {
	Transaction Transaction
}

// LoadTransactionRepositories ...
func LoadTransactionRepositories(
	postgres *gorm.DB,
) TransactionRepositories {
	return TransactionRepositories{
		Transaction: BuildTransactionPostgres(postgres),
	}
}
