package service

import (
	"bank/internal/transaction-system/data-provider/client/grpc"
	"bank/internal/transaction-system/data-provider/repository"
	"bank/internal/transaction-system/domain"
	"errors"
	"log"
	"time"

	"github.com/google/uuid"
)

// TransactionService ...
type TransactionService struct {
	transactionRepository repository.TransactionRepositories
	userGrpcClient        grpc.TransactionUserGRPCClient
	walletGrpcClient      grpc.TransactionWalletGRPClient
	timeNow               func() time.Time
}

// NewTransactionService ...
func NewTransactionService(
	transactionRepository repository.TransactionRepositories,
	userGrpcClient grpc.TransactionUserGRPCClient,
	walletGrpcClient grpc.TransactionWalletGRPClient,
	timeNow func() time.Time,
) *TransactionService {
	return &TransactionService{
		transactionRepository: transactionRepository,
		userGrpcClient:        userGrpcClient,
		walletGrpcClient:      walletGrpcClient,
		timeNow:               timeNow,
	}
}

// Create ...
func (t *TransactionService) Create(transaction domain.Transaction) (string, error) {
	existUser, err := t.userGrpcClient.UserExists(
		transaction.UserID,
	)
	if err != nil {
		log.Printf("\nfunc:Create(Transaction.Service) UserExists GRPC error: %+v", err)

		return "", err
	}
	if existUser != true {
		log.Printf("\nfunc:Create(Transaction.Service) user not exists, user_id: %+v", transaction.UserID)

		return "", errors.New("user not exists")
	}

	withdrawn, err := t.walletGrpcClient.Withdrawal(
		transaction.UserID,
		float32(transaction.Amount),
	)
	if err != nil {
		log.Printf("\nfunc:Create(Transaction.Service) Withdrawal GRPC error: %+v", err)

		return "", err
	}
	if withdrawn != true {
		log.Printf("\nfunc:Create(Transaction.Service) User cannot have balance user_id: %+v", transaction.UserID)

		return "", errors.New("user without balance")
	}

	transaction.ID = uuid.New().String()
	transaction.CreatedAt = t.timeNow()
	id, err := t.transactionRepository.Transaction.Create(transaction)
	if err != nil {
		log.Printf("\nfunc:Create(Transaction.Service) Create Repository error: %+v", err)

		return "", err
	}

	return id, nil
}
