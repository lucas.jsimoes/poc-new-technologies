package middleware

import (
	"context"
	"log"
	"net/http"

	"github.com/google/uuid"
	"github.com/urfave/negroni"
)

const RequestID = "requestID"

func EventTrackingRest() negroni.Handler {
	return negroni.HandlerFunc(func(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		ctx := r.Context()
		requestID := uuid.New().String()
		ctx = context.WithValue(ctx, "requestID", requestID)

		log.SetPrefix("\nrequestID = " + requestID + "\n")

		next(rw, r.WithContext(ctx))
	})
}
