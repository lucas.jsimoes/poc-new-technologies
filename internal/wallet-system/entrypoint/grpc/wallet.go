package grpc

import (
	"bank/internal/wallet-system/domain"
	walletproto "bank/internal/wallet-system/entrypoint/grpc/proto"
	"bank/internal/wallet-system/service"
	"context"
	"log"
)

// WalletServiceServer ...
type WalletServiceServer struct {
	walletproto.UnimplementedWalletServiceServer
	service *service.WalletService
}

// NewWalletServiceServer ...
func NewWalletServiceServer(
	service *service.WalletService,
) *WalletServiceServer {
	return &WalletServiceServer{
		service: service,
	}
}

// Withdrawal ...
func (s *WalletServiceServer) Withdrawal(
	ctx context.Context, req *walletproto.WalletRequest,
) (*walletproto.WalletResponse, error) {
	//time.Sleep(8 * time.Second) test instance requestID
	log.Printf("func:Withdraw(Wallet.GRPC) UserID: %s", req.UserID)

	withdrawn, err := s.service.Withdrawal(domain.Wallet{
		UserID: req.UserID,
		Amount: float64(req.Amount),
	})

	return &walletproto.WalletResponse{
		Withdrawn: withdrawn,
	}, err
}
