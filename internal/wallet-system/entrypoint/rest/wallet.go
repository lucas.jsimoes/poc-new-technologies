package rest

import (
	"bank/internal/wallet-system/service"

	"github.com/gorilla/mux"
)

// Wallet ...
type Wallet struct {
	service *service.WalletService
	router  *mux.Router
}

// NewWalletRest ...
func NewWalletRest(
	service *service.WalletService,
	router *mux.Router,
) *Wallet {
	subRouter := router.PathPrefix("/wallet").Subrouter()
	return &Wallet{
		service: service,
		router:  subRouter,
	}
}

// RegisterRoutes ...
func (w Wallet) RegisterRoutes() {
	w.router.HandleFunc(
		"/add-balance", w.addBalance,
	).Headers(
		"Content-Type", "application/json",
	).Methods("POST")

}
