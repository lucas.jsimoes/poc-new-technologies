package rest

import (
	"bank/internal/wallet-system/domain"
	"bank/internal/wallet-system/entrypoint/rest/event"
	"encoding/json"
	"log"
	"net/http"
)

// addBalance ...
func (t *Wallet) addBalance(
	w http.ResponseWriter, r *http.Request,
) {
	var err error
	var payload event.Wallet

	decoder := json.NewDecoder(r.Body)
	if err = decoder.Decode(&payload); err != nil {
		log.Printf("\nfunc:addBalance(Wallet.Rest) invalid request body, error: %+v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	err = t.service.AddBalance(walletToDomain(payload))
	if err != nil {
		log.Printf("\nfunc:addBalance(Wallet.Rest) called service err: %+v", err)

		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
	return
}

// walletToDomain ...
func walletToDomain(payload event.Wallet) domain.Wallet {
	return domain.Wallet{
		UserID: payload.UserID,
		Amount: payload.Amount,
	}
}
