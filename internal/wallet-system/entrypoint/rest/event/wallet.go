package event

// Wallet ...
type Wallet struct {
	UserID string  `json:"user_id"`
	Amount float64 `json:"amount"`
}
