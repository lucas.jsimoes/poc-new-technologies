package service

import (
	"bank/internal/wallet-system/data-provider/repository"
	"bank/internal/wallet-system/domain"
	"fmt"
	"log"
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

// WalletService ...
type WalletService struct {
	walletRepository repository.WalletRepositories
	timeNow          func() time.Time
}

// NewWalletService ...
func NewWalletService(
	walletRepository repository.WalletRepositories,
	timeNow func() time.Time,
) *WalletService {
	return &WalletService{
		walletRepository: walletRepository,
		timeNow:          timeNow,
	}
}

// AddBalance ...
func (w *WalletService) AddBalance(wallet domain.Wallet) error {
	var now = w.timeNow()
	walletCurrent, err := w.walletRepository.Wallet.FindBalanceByUserID(
		wallet.UserID,
	)
	if err != nil && err != gorm.ErrRecordNotFound {
		log.Printf("\nfunc:AddBalance(Wallet.Service) FindBalanceByUserID repository error: %+v", err)

		return err
	}

	if walletCurrent == (domain.Wallet{}) {
		wallet.ID = uuid.New().String()
		wallet.CreatedAt = now
		wallet.UpdatedAt = now
		_, err := w.walletRepository.Wallet.Create(wallet)
		if err != nil {
			log.Printf("\nfunc:AddBalance(Wallet.Service) Create Wallet repository error: %+v", err)

			return err
		}

		return nil
	}

	walletCurrent.UpdatedAt = now
	walletCurrent.Amount += wallet.Amount
	err = w.walletRepository.Wallet.UpdateBalance(walletCurrent)
	if err != nil {
		log.Printf("\nfunc:AddBalance(Wallet.Service) UpdateBalance repository error: %+v", err)

		return err
	}

	return nil
}

// Withdrawal ...
func (w *WalletService) Withdrawal(wallet domain.Wallet) (bool, error) {
	var now = w.timeNow()
	log.Printf("\nfunc:Withdrawal(Wallet.Service) userID: %+v", wallet.UserID)
	walletCurrent, err := w.walletRepository.Wallet.FindBalanceByUserID(
		wallet.UserID,
	)
	if err != nil && err != gorm.ErrRecordNotFound {
		log.Printf("\nfunc:Withdrawal(Wallet.Service) FindBalanceByUserID repository error: %+v", err)

		return false, err
	}

	if walletCurrent == (domain.Wallet{}) || walletCurrent.Amount < wallet.Amount {
		log.Printf("\nfunc:Withdrawal(Wallet.Service) User without balance user_id: %+v", wallet.UserID)

		return false, fmt.Errorf("user without balance")
	}

	walletCurrent.UpdatedAt = now
	walletCurrent.Amount -= wallet.Amount
	err = w.walletRepository.Wallet.UpdateBalance(walletCurrent)
	if err != nil {
		log.Printf("\nfunc:Withdrawal(Wallet.Service) UpdateBalance repository error: %+v", err)

		return false, err
	}

	return true, nil
}
