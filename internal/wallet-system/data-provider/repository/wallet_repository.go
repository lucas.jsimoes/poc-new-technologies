package repository

import (
	"gorm.io/gorm"
)

// WalletRepositories ...
type WalletRepositories struct {
	Wallet Wallet
}

// LoadWalletRepositories ...
func LoadWalletRepositories(
	postgres *gorm.DB,
) WalletRepositories {
	return WalletRepositories{
		Wallet: BuildWalletPostgres(postgres),
	}
}
