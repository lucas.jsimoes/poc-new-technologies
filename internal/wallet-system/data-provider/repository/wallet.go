package repository

import (
	"bank/internal/wallet-system/domain"

	"gorm.io/gorm"
)

// Wallet ...
type Wallet interface {
	UpdateBalance(wallet domain.Wallet) error

	Create(wallet domain.Wallet) (string, error)

	FindBalanceByUserID(userID string) (domain.Wallet, error)
}

// WalletPostgres ...
type WalletPostgres struct {
	db *gorm.DB
}

// BuildWalletPostgres ...
func BuildWalletPostgres(
	db *gorm.DB,
) WalletPostgres {
	return WalletPostgres{db: db}
}

// UpdateBalance ...
func (w WalletPostgres) UpdateBalance(wallet domain.Wallet) error {
	result := w.db.Save(&wallet)

	return result.Error
}

// Create ...
func (w WalletPostgres) Create(wallet domain.Wallet) (string, error) {
	result := w.db.Create(&wallet)
	if result.Error != nil {
		return "", result.Error
	}

	return wallet.ID, result.Error
}

// FindBalanceByUserID ...
func (w WalletPostgres) FindBalanceByUserID(userID string) (domain.Wallet, error) {
	var wallet domain.Wallet

	result := w.db.Where("user_id = ?", userID).First(&wallet)
	if result.Error != nil {
		return wallet, result.Error
	}

	return wallet, result.Error
}
