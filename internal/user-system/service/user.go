package service

import (
	"bank/internal/user-system/data-provider/repository"
	"bank/internal/user-system/domain"
	"log"
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

// UserService ...
type UserService struct {
	userRepository repository.UserRepositories
	timeNow        func() time.Time
}

// NewUserService ...
func NewUserService(
	userRepository repository.UserRepositories,
	timeNow func() time.Time,
) *UserService {
	return &UserService{
		userRepository: userRepository,
		timeNow:        timeNow,
	}
}

// Create ...
func (u *UserService) Create(user domain.User) (string, error) {
	user.ID = uuid.New().String()
	log.Printf("\nfunc:Create(User.Service) userID: %+v", user.ID)
	user.CreatedAt = u.timeNow()
	id, err := u.userRepository.User.Create(user)
	if err != nil {
		log.Printf("\nfunc:Create(User.Service) Create Repository error: %+v", err)

		return "", err
	}

	return id, nil
}

// Exists ...
func (u *UserService) Exists(id string) (bool, error) {
	user, err := u.userRepository.User.FindByID(id)
	if err != nil && err != gorm.ErrRecordNotFound {
		log.Printf("\nfunc:Exists(User.Service) FindByID Repository error: %+v", err)

		return false, err
	}

	if user == (domain.User{}) {
		log.Printf("\nfunc:Exists(User.Service) user not exists user_id: %+v", id)

		return false, nil
	}

	return true, nil
}
