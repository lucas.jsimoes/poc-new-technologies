package repository

import (
	"bank/internal/user-system/domain"

	"gorm.io/gorm"
)

// User ...
type User interface {
	Create(user domain.User) (string, error)

	FindByID(id string) (domain.User, error)
}

// UserPostgres ...
type UserPostgres struct {
	db *gorm.DB
}

// BuildUserPostgres ...
func BuildUserPostgres(
	db *gorm.DB,
) UserPostgres {
	return UserPostgres{db: db}
}

// Create ...
func (u UserPostgres) Create(user domain.User) (string, error) {
	result := u.db.Create(&user)
	if result.Error != nil {
		return "", result.Error
	}

	return user.ID, result.Error
}

// FindByID ...
func (u UserPostgres) FindByID(id string) (domain.User, error) {
	var user domain.User

	result := u.db.Where("id = ?", id).First(&user)
	if result.Error != nil {
		return user, result.Error
	}

	return user, result.Error
}
