package repository

import (
	"gorm.io/gorm"
)

// UserRepositories ...
type UserRepositories struct {
	User User
}

// LoadUserRepositories ...
func LoadUserRepositories(
	postgres *gorm.DB,
) UserRepositories {
	return UserRepositories{
		User: BuildUserPostgres(postgres),
	}
}
