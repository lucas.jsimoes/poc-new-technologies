package rest

import (
	"bank/internal/user-system/domain"
	"bank/internal/user-system/entrypoint/rest/event"
	"encoding/json"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

// create ...
func (u *User) create(
	w http.ResponseWriter, r *http.Request,
) {
	var err error
	var payload event.User

	decoder := json.NewDecoder(r.Body)
	if err = decoder.Decode(&payload); err != nil {
		log.Printf("\nfunc:create(User.Rest) invalid request body, error: %+v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	id, err := u.service.Create(userToDomain(payload))
	if err != nil {
		log.Printf("\nfunc:create(User.Rest) called service err: %+v", err)

		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Content-Type", "application/json")

	json.NewEncoder(w).Encode(id)
	return
}

// exists ...
func (u *User) exists(
	w http.ResponseWriter, r *http.Request,
) {
	vars := mux.Vars(r)
	id := strings.TrimSpace(vars["id"])

	exists, err := u.service.Exists(id)
	if err != nil {
		log.Printf("\nfunc:exists(User.Rest) called service err: %+v", err)

		w.WriteHeader(http.StatusInternalServerError)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(exists)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(exists)
	return
}

// userToDomain ...
func userToDomain(payload event.User) domain.User {
	return domain.User{
		Cpf:  payload.Cpf,
		Name: payload.Name,
	}
}
