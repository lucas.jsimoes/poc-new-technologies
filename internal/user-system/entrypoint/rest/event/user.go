package event

// User ...
type User struct {
	Cpf  string `json:"cpf"`
	Name string `json:"name"`
}
