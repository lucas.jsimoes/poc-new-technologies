package rest

import (
	"bank/internal/user-system/service"

	"github.com/gorilla/mux"
)

// User ...
type User struct {
	service *service.UserService
	router  *mux.Router
}

// NewUserRest ...
func NewUserRest(
	service *service.UserService,
	router *mux.Router,
) *User {
	subRouter := router.PathPrefix("/user").Subrouter()
	return &User{
		service: service,
		router:  subRouter,
	}
}

// RegisterRoutes ...
func (u User) RegisterRoutes() {
	u.router.HandleFunc(
		"", u.create,
	).Headers(
		"Content-Type", "application/json",
	).Methods("POST")

	u.router.HandleFunc(
		"/exists/{id}", u.exists,
	).Methods("GET")
}
