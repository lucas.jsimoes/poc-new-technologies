package grpc

import (
	userproto "bank/internal/user-system/entrypoint/grpc/proto"
	"bank/internal/user-system/service"
	"context"
	"log"
)

// UserServiceServer ...
type UserServiceServer struct {
	userproto.UnimplementedUserServiceServer
	service *service.UserService
}

// NewUserServiceServer ...
func NewUserServiceServer(
	service *service.UserService,
) *UserServiceServer {
	return &UserServiceServer{
		service: service,
	}
}

// UserExists ...
func (s *UserServiceServer) UserExists(
	ctx context.Context, req *userproto.UserRequest,
) (*userproto.UserResponse, error) {
	log.Printf("\nfunc:UserExists(User.GRPC) UserID: %s", req.UserID)

	exists, err := s.service.Exists(req.UserID)

	return &userproto.UserResponse{
		Exists: exists,
	}, err
}
