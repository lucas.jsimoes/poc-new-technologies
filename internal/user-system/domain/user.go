package domain

import "time"

// User ...
type User struct {
	ID        string    `json:"id"`
	Cpf       string    `json:"cpf"`
	Name      string    `json:"name"`
	CreatedAt time.Time `json:"created_at"`
}

func (User) TableName() string {
	return "user_system.user"
}
