######################################################################################
# ENVIRONMENT
######################################################################################
create-data-volumes-folder:
	@mkdir -p ~/.poc-bank/postgres/data

env-up: create-data-volumes-folder
	docker-compose -f environment/docker-compose.yml up --remove-orphans

env-up-d: create-data-volumes-folder
	docker-compose -f environment/docker-compose.yml up -d --remove-orphans

env-down:
	docker-compose -f environment/docker-compose.yml down --remove-orphans

######################################################################################
# GRPC
######################################################################################
generate-grpc-out:
	@protoc --go_out=. --go_opt=paths=source_relative internal/wallet-system/entrypoint/grpc/proto/wallet.proto

generate-grpc:
	@protoc --go-grpc_out=. --go-grpc_opt=paths=source_relative internal/wallet-system/entrypoint/grpc/proto/wallet.proto
